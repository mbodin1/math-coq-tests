(* Fichier Coq écrit pendant la réunion. *)

From Coq Require Import Arith_base ZArith ZArith.Zpower.

Local Open Scope Z_scope.

Require Import Lia.

(1) définir la puissance à partie d’axiomes.

Lemma eq1 : forall a : Z,
 a^4 + 4 = (a^2 - 2*a + 2) * (a^2 + 2*a + 2).
Proof.
  lia.
Qed.

Ltac rewrite_by a b :=
  let E := fresh "E" in
  assert (E : a = b);
  [ lia
  | repeat rewrite E; clear E ].

Lemma eq2 : forall a : Z,
 a^4 + 4 = (a^2 - 2*a + 2) * (a^2 + 2*a + 2).
Proof.
  intro a.
  repeat rewrite Zmult_plus_distr_r.
  repeat rewrite Zmult_plus_distr_l.
  repeat rewrite Zmult_minus_distr_r.
  repeat rewrite Z.mul_assoc.
  unfold Z.pow; unfold Z.pow_pos; unfold Pos.iter; repeat rewrite Z.mul_1_r.
  repeat rewrite Z.mul_assoc.
  rewrite_by (a * a * 2 * a) (2 * a * a * a).
  repeat rewrite Z.add_assoc.
  repeat rewrite Z.add_sub_assoc.

  (* Search "(( _ + _ ) * ( _ - _ ))%Z". *)
Abort.

Lemma eq3 : forall a : Z,
 a^4 + 4 = (a^2 - 2*a + 2) * (a^2 + 2*a + 2).
Proof.
  intro a.
  set (A := a^2 + 2).
  set (B := 2*a).
  rewrite_by (a^2 -B + 2) (A - B).
  rewrite_by (a^2 + B + 2) (A + B).

Abort.

Lemma eq4 : forall a : Z,
 a <> 1 ->
 a^2 - 2*a + 2 <> 1.
Proof.

(1) par contraposition. Factoriser a^2 - 2*a + 1
(2) transivité avec 2 et prendre le cas a positif.

  intro a.
  Check contrapos.
  intro D.



