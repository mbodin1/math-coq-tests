
(* MathC2+ 2023 *)

(* Règles de déductions logiques. *)

Axiom règle_k : forall f g : Prop, f -> (g -> f).
Axiom règle_s : forall f g h : Prop, (f -> (g -> h)) -> ((f -> g) -> (f -> h)).
Axiom contraposition : forall f g : Prop, (~ f -> ~ g) -> (g -> f).
Axiom pour_tout_implique : forall T f (g : T -> Prop), (forall x, (f -> g(x))) -> (f -> forall x, g(x)).
Axiom spécialisation : forall T (a : T) f, (forall x, f(x)) -> f(a).
Axiom modus_ponens : forall f g : Prop, (f /\ (f -> g)) -> g.


























(* Socrate est mortel *)

Parameter chose : Type.
Parameter homme : chose -> Prop.
Parameter mortel : chose -> Prop.
Parameter Socrate : chose.

Axiom tous_les_hommes_sont_mortels : forall x, homme(x) -> mortel(x).
Axiom Socrate_est_un_homme : homme(Socrate).

Theorem Socrate_est_mortel : mortel(Socrate).
Proof.
  pose proof tous_les_hommes_sont_mortels Socrate.
  pose proof H Socrate_est_un_homme.
  exact H0.
Qed.



















(* Si quelqu'un aime, alors tout le monde l'aime. *)

Parameter personne : Type.
Parameter aime : personne -> personne -> Prop.
Infix "♥" := aime (at level 50).

Axiom tout_le_monde_aime_quelquun_qui_aime :
  forall x, ((exists y, x ♥ y) -> (forall z, z ♥ x)).

Parameter Alice Bob Charlie David : personne.  

Axiom Alice_aime_Bob : Alice ♥ Bob.

Theorem Charlie_aime_David : Charlie ♥ David.
Proof.
  assert (forall z, z ♥ Alice).
  - apply tout_le_monde_aime_quelquun_qui_aime.
    exists Bob.
    exact Alice_aime_Bob.
  - pose proof H David.
    assert (forall z, z ♥ David).
    + apply tout_le_monde_aime_quelquun_qui_aime.
      exists Alice.
      exact H0.
    + pose proof H1 Charlie.
      exact H2.
Qed.
