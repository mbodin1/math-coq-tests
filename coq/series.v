
Pour toute série f qui converge, alors la série f² converge elle aussi.

Preuve : comme f converge, à partir d’un moment, f(x) < 1, et donc f²(x) est plus petit.

