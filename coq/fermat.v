
Require Import lib.


Theorem fermat : forall p,
  p >= 3 ->
  ~ exists x y z,
    x ≠ 0 /\ y ≠ 0
    /\ x^p + y^p = z^p.
Admitted.
















Theorem weak_fermat : forall p,
  exists x y z,
    x ≠ 0
    /\ x^p + y^p = z^p.
Proof.
  intro p.
  exists 2, 0, 2.
  split.
  - discriminate.
  - rewrite Z.pow_0_l.
    + rewrite Z.add_0_r. reflexivity. (* lia. *)
    + 
Abort.
















Theorem weak_fermat : forall p,
  0 < p ->
  exists x y z,
    x ≠ 0
    /\ x^p + y^p = z^p.
Proof.
  intro p.
  intro H.
  exists 2, 0, 2.
  split.
  - discriminate.
  - rewrite Z.pow_0_l.
    + rewrite Z.add_0_r. reflexivity.
    + exact H.
Qed.











Theorem weak_fermat2 : forall p,
  0 < p ->
  exists x y z,
    x ≠ 0
    /\ x^p + y^p = z^p.
Proof.
  Soit p.
  On suppose (0 < p).
  On pose x = 2.
  On pose y = 0.
  On pose z = 2.
  split.
  - Par calcul.
  - On réécrit (0 ^ p) en 0.
    Par calcul.
Qed.









