
(** Un exemple de construction (très naïve) de réels **)

Definition digit := { n : nat | 0 <= n <= 9 }.

Definition digit_to_nat : digit -> nat := @proj1_sig _ _.
Coercion digit_to_nat : digit >-> nat.

Definition digit_eq (d1 d2 : digit) :=
  (d1 : nat) = (d2 : nat).

Definition real_stream : Type := nat * (nat -> digit).

Definition nth_digit : nat -> real_stream -> digit := fun n '(i, ds) => ds n.

(** Les nombres réels dont l’écriture se finissent par une infinité de 9 peuvent
  aussi être écrits avec une infinité de 0. **)
Record writing_variation r1 r2 m := {
    writing_variation_same_before :
      forall n, m <= m -> digit_eq (nth_digit n r1) (nth_digit n r2) ;
    writing_variation_at_position :
      (nth_digit m r1 : nat) = 1 + (nth_digit m r2 : nat)
    writing_variation_after_0 :
    writing_variation_after_9 :
  }.

Definition real_stream_eq r1 r2 :=
  (forall n, digit_eq (Str_nth n r1) (Str_nth n r2))
  \/ (exists m,
       (forall n, n <= m -> digit_eq (Str_nth n r1) (Str_nth n r2)))
       /\ (Str_nth m r1 : nat) = 1 + Str_nth m r2).
       /\ (forall n, n > m -> )).
