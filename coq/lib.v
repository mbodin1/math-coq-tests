
From Coq Require Export BinInt.
From Coq Require Export Lia.

Open Scope Z_scope.

Notation "x ≠ y" := (x <> y) (at level 10).

Definition unique {A} (P : A -> Prop) :=
  forall x y, P x -> P y -> x = y.

Notation "'exists' '!' x ',' P" :=
  (unique (fun x => P) /\ exists x, P) (at level 200) : type_scope.

Notation "'exists' '!' x ':' T ',' P" :=
  (unique (fun x : T => P) /\ exists x : T, P) (at level 200) : type_scope.

Definition set A := A -> Prop.

Class BagIn A E := {
    in_op : A -> E -> Prop
  }.

Instance BagIn_set : forall A, BagIn A (set A) :=
  fun A => {| in_op := fun a (E : set A) => E a |}.

Notation "x '\in' E" := (in_op x E) (at level 40).

Tactic Notation "test" simple_intropattern(H) ":" constr(P) :=
  let D := fresh "dec" in
  assert (D : {P} + {~P}); [
      solve [ lia
            | eapply Bool.reflect_dec;
              solve [
                  apply Z.eqb_spec
                | apply Z.ltb_spec0
                | apply Z.leb_spec0
                | apply PeanoNat.Nat.ltb_spec0
                | apply PeanoNat.Nat.leb_spec0 ]
            | constructor; eauto ]
    | destruct D as [H|H] ].


Tactic Notation "Soit" simple_intropattern(H) :=
  intro H.

Tactic Notation "Soient" simple_intropattern_list_sep(L, ",") :=
  intros L.

Tactic Notation "On" "suppose" constr(P) :=
  lazymatch goal with
  | |- P -> _ => intro
  | |- ~ P => intro
  | |- ?H -> _ => fail "Il était attendu de supposer " H
  | _ => fail "Le but n’est pas une implication."
  end.

Ltac get_hyp P :=
  let T := type of P in
  lazymatch goal with
  | H : P |- _ => constr:(H)
  | P : T |- _ => constr:(P)
  end.

Ltac decompose_hyp H :=
  lazymatch type of H with
  | _ /\ _ =>
    let H1 := fresh H in
    let H2 := fresh H in
    destruct H as (H1&H2);
    decompose_hyp H1;
    decompose_hyp H2
  | exists x, _ =>
    let y := fresh x in
    let H' := fresh H in
    destruct H as (y&H');
    decompose_hyp H'
  | _ => idtac
  end.

Ltac get_function f :=
  lazymatch f with
  | ?g _ =>
    let r := get_function g in
    constr:(r)
  | _ => constr:(f)
  end.

Tactic Notation "Décomposer" constr(P) :=
  first [
    let H := get_hyp P in
    first [
        progress decompose_hyp H
      | let f := get_function P in
        unfold f in H;
        progress decompose_hyp H
      | let P := type of P in
        let f := get_function P in
        unfold f in H;
        progress decompose_hyp H
      | idtac ]
  | let H := fresh in
    pose (H := P);
    clearbody H;
    progress decompose_hyp H ].

Tactic Notation "Absurde" constr(P) :=
  let H := get_hyp P in
  exfalso; generalize H; try clear H;
  try lazymatch goal with
  | |- ?x = ?y -> False => fold (x <> y)
  end.

Tactic Notation "On" "pose" ident(x) "=" constr(t) :=
  let rec aux :=
    lazymatch goal with
    | |- exists x, _ => exists t
    | |- ?g =>
      let f := get_function g in
      unfold f;
      aux
    end in
  aux || fail "Le but n’est pas une existentielle.".

Tactic Notation "Par" "hypothèse" :=
  solve [ assumption | auto ].

Tactic Notation "Par" "calcul" :=
  try rewrite Z.pow_0_l;
  try rewrite Z.pow_1_l;
  try rewrite Z.pow_0_r;
  try rewrite Z.pow_1_r;
  let go :=
    solve [
        discriminate
      | auto with zarith
      | subst; auto with zarith
      | lia ] in
  solve [ go | simpl in *; go ].

Tactic Notation "Par" "analyse" "de" "cas" "sur" constr(P) :=
  destruct P;
  first [ Par hypothèse
        | exfalso; Par hypothèse
        | exfalso; Par calcul
        | idtac ].

Tactic Notation "On" "réécrit" constr(a) "en" constr(b) :=
  let E := fresh "E" in
  assert (E : a = b); [ Par calcul | rewrite E; clear E ].


Parameter point : Type.
Parameter line : Type.
Parameter line_to_set : line -> set point.

Instance BagIn_line : BagIn point line :=
  {| in_op := fun p d => p \in line_to_set d |}.


Require Export List.

Instance BagIn_list : forall A, BagIn A (list A) :=
  fun A => {| in_op := fun a l => In a l |}.

Notation "| l |" := (length l) (at level 40).

Coercion Z.of_nat : nat >-> Z.


Ltac to_nat n :=
  lazymatch n with
  | Z.of_nat ?n => constr:(n)
  | _ =>
    lazymatch type of n with
    | Z => constr:(Z.to_nat n)
    | N => constr:(BinNatDef.N.to_nat n)
    | positive => constr:(Pos.to_nat n)
    | nat => constr:(n)
    end
  end.

Tactic Notation "Par" "récurrence" "sur" ident(n) ">=" constr(z) ","
    "on" "montre" "que" uconstr(P) :=
  let z := to_nat z in
  let L := fresh "L" in
  let zs := eval compute in z in
  let R := fresh "R" in
  assert (R : (z = zs)%nat); [ Par calcul |];
  assert (L : forall n : nat, P (n + z)%nat);
  [ induction n; [
      try rewrite PeanoNat.Nat.add_0_l;
      try rewrite R in *; clear R
    | try rewrite plus_Sn_m, plus_n_Sm;
      try rewrite R in *; clear R;
      let us := eval compute in (z + 1)%nat in
      try On réécrit (S z)%nat en us
    ]
  | simpl in L ].
