
Axiom tier_exclu : forall P, P \/ ~ P.

Axiom f : False.

Definition trivial x y z := x = 0 /\ y = 0 /\ z = 0.

Fixpoint power x p :=
  match p with
  | 0 => 1
  | S p' => x * power x p'
  end.

Axiom choice : forall A B (P : A -> B -> Prop),
  (forall x, exists y, P x y) ->
  (exists f, forall x, P x (f x)).

Theorem fermat : forall p,
  p >= 3 ->
  ~ exists x y z, ~ trivial x y z
                  /\ power x p + power y p = power z p.
Proof.
  intro p.
  intro p3.
  (* Show Proof. *)
  set (D := tier_exclu (exists x y z : nat,
    ~ trivial x y z /\ power x p + power y p = power z p)).
  clearbody D.
  inversion D as [V|F].
  - destruct V as (x&y&z&N&E).
    exfalso. exact f.
  - exact F.
Qed.

Print fermat.

Print Assumptions fermat.

Theorem fermat2 : forall p,
  p >= 3 ->
  ~ exists x y z, ~ trivial x y z
                  /\ power x p + power y p = power z p.
Proof.
  exfalso. exact f.
Qed.

Theorem fermat3 : forall p,
  p >= 3 ->
  ~ exists x y z, ~ trivial x y z
                  /\ power x p + power y p = power z p.
Proof.
  intro p.
  intro p3.
  intro V.
  destruct V as (x&y&z&N&E).
  exfalso. exact f.
Qed.

Print Assumptions fermat3.


From Coq Require Import Lia.

Section Sqrt2.

Definition divide (a b : nat) := exists c : nat, a * c = b.
Notation "( a | b )" := (divide a b) (at level 0).

Lemma mul_divide : forall a b,
  (a | a * b).
Proof.
  intros a b. exists b. reflexivity.
Qed.

Definition prime p :=
  p <> 1 /\ forall q, (q | p) -> q = 1 \/ q = p.

Hypothesis prime_2 : prime 2.

Hypothesis prime_divide : forall p a b,
  prime p ->
  (p | a * b) ->
  (p | a) \/ (p | b).

Variable real : Type.

Variable nat_to_real : nat -> real.
Coercion nat_to_real : nat >-> real.

Hypothesis nat_to_real_inj : forall n1 n2,
  nat_to_real n1 = nat_to_real n2 -> n1 = n2.

Variable mul_real : real -> real -> real.
Hypothesis mul_nat : forall n1 n2 : nat,
  mul_real n1 n2 = n1 * n2.
Notation "a * b" := (mul_real a b).

Hypothesis mul_comm : forall a b, a * b = b * a.
Hypothesis mul_assoc : forall a b c, (a * b) * c = a * (b * c).

Variable le_real : real -> real -> Prop.
Hypothesis le_nat : forall n1 n2 : nat,
  le_real n1 n2 <-> n1 <= n2.
Notation "a <= b" := (le_real a b).

Definition positive := { x : real | 0 <= x }.

Check (exist (fun x => 0 <= x) (42 : real) ltac:(apply le_nat; lia) : positive).

Definition positive_to_real : positive -> real :=
  fun p => proj1_sig p.
Coercion positive_to_real : positive >-> real.

Definition nat_to_positive : nat -> positive :=
  fun n => exist (fun x => 0 <= x) n ltac:(apply le_nat; lia).
Coercion nat_to_positive : nat >-> positive.

Variable sqrt : positive -> real.
Hypothesis sqrt_pos : forall p,
  0 <= sqrt p.
Hypothesis sqrt_square : forall p,
  sqrt p * sqrt p = p.

Definition rational (x : real) :=
  exists p q : nat, q <> 0 /\ x * q = p.

Hypothesis normal_form : forall x,
  rational x ->
  exists p q : nat, q <> 0 /\ x * q = p /\ forall n, prime n -> ~ ((n | q) /\ (n | p)).

Theorem square_two_irrational : ~ rational (sqrt 2).
Proof.
  intro R. (* Supposons que sqrt 2 est rationnel. *)
  destruct (normal_form _ R) as (p&q&D&E1&M). (* Supposons p, q, tels que sqrt 2 * q = p (E1) et p, q minimaux (M). *)
  assert (E2: (sqrt 2 * sqrt 2) * (q * q) = p * p). (* On élève E1 au carré tout en réordonnant les termes (E2). *)
  { rewrite <- E1. repeat rewrite mul_assoc. rewrite (mul_comm q (sqrt 2 * q)).
    rewrite mul_assoc. reflexivity. }
  rewrite sqrt_square in E2. (* On simplifie la racine carré dans E2. *)
  simpl in E2. repeat rewrite mul_nat in E2. apply nat_to_real_inj in E2. (* E2 est maintenant une équation d’entiers naturels et non plus de réels. *)
  assert (D1: (2 | p * p)). (* Donc 2 divide p * p (D1). *)
  { rewrite <- E2. apply mul_divide. }
  assert (D2: (2 | p)). (* Donc 2 divide p (D2) car 2 est premier. *)
  { destruct (prime_divide _ _ _ prime_2 D1); assumption. }
  set (D2':=D2). destruct D2' as [p' E3]. (* Soit donc p' tel que p = 2 * p' (E3). *)
  assert (E4: (q * q = 2 * p' * p')%nat); [ lia |]. (* On a donc q * q = 2 * p' * p' *)
  assert (D3: (2 | q * q)). (* On a donc 2 divide q * q (D3). *)
  { exists (p' * p')%nat. lia. }
  assert (D4: (2 | q)). (* Donc 2 divide q (D4) car 2 est premier. *)
  { destruct (prime_divide _ _ _ prime_2 D3); assumption. }
  apply (M _ prime_2); auto. (* On avait supposé p et q minimaux, mais D2 et D4 montrent le contraire. *)
Qed.

End Sqrt2.


From Coq Require Import Lia.

Definition digit := { n | 0 <= n <= 9 }.

Check (exist (fun n => 0 <= n <= 9) 4 ltac:(split; lia) : digit).

Definition real_interne : Type := nat * (nat -> digit). (* 10^n * 0,d₁d₂d₃... *)

Definition real := { x : real_interne | proj1_sig (snd x 0) <> 0 /\ ~ exists m, forall i, i > m -> proj1_sig (snd x i) = 9 }.

Inductive eq_real : real -> real -> Prop :=
  | eq_real_refl : forall x, eq_real x x
  | eq_real_incr : forall (r1 r2 : real) n f1 f2,
      proj1_sig r1 = (n, f1) /\ proj1_sig r2 = (n + 1, f2) /\ proj1_sig (f2 0) = 0 /\ (forall i, f1 i = f2 (i + 1)) -> eq_real r1 r2
  .


