
Axiom tier_exclu : forall P, P \/ ~ P.

Lemma pierce : forall A B : Prop, ((A -> B) -> A) -> A.
Proof.
intros A B.
destruct (tier_exclu A) as [pA|pNA].
- intros ?. assumption.
- intro I. apply I.
  intro pA.
  exfalso.
  unfold not in pNA.
  exact (pNA pA).
Qed.

Lemma pierce' : forall A B : Prop, ((A -> B) -> A) -> A.
Proof.
intros A B.
destruct (tier_exclu A) as [pA|pNA].
- auto.
- intro I.
  assert (A -> B).
  { intro pA.
    pose proof pNA pA as pF.
    destruct pF.
  }
  pose proof I H as a.
  exact a.
Qed.


