
Require Import lib.

Section Sqrt2.

Open Scope nat_scope.

Definition divide (a b : nat) := exists c : nat, a * c = b.
Notation "( a | b )" := (divide a b) (at level 0).

Lemma transivity_of_formulas : forall A B C : Prop,
  ((A -> B) /\ (B -> C)) ->
  (A -> C).
Proof.
  Soient A, B, C.
  On suppose ((A -> B) /\ (B -> C)).
  On suppose A.
  Décomposer H.
  auto.
Qed.
(*
  apply H2.
  apply H1.
  assumption.

  pose (H3 := H1 H0).
  pose (H4 := H2 H3).
  exact H4.
Qed.
*)

Lemma mul_divide : forall a b,
  (a | a * b).
Proof.
  Soient a, b.
  unfold divide.
  On pose c = b.
  reflexivity.
Qed.

Definition prime p :=
  p > 1 /\ forall q, (q | p) -> q = 1 \/ q = p.

Lemma divide_lt : forall a b, b ≠ 0 -> (a | b) -> a <= b.
Proof.
  Soient a, b.
  On suppose (b ≠ 0).
  On suppose (a | b).
  Décomposer (a | b).
  On réécrit a en (a * 1).
  On réécrit b en (a * c).
  apply Mult.mult_le_compat_l. (* forall n m p : nat, n <= m -> p * n <= p * m *)
  Par analyse de cas sur c.
  Par calcul.
Qed.

Lemma prime_2 : prime 2.
Proof.
  split.
  - Par calcul.
  - Soit q.
    On suppose (q | 2).
    assert (q <= 2).
    { apply divide_lt.
      - Par calcul.
      - Par hypothèse. }
    inversion H0.
    + Par calcul.
    + inversion H2.
      * Par calcul.
      * inversion H4.
        subst.
        Décomposer (0 | 2).
        Absurde (0 * c = 2).
        Par calcul.
Qed.

Hypothesis prime_divide : forall p a b,
  prime p ->
  (p | a * b) ->
  (p | a) \/ (p | b).

Variable real : Type.

Variable nat_to_real : nat -> real.
Coercion nat_to_real : nat >-> real.

Hypothesis nat_to_real_inj : forall n1 n2,
  nat_to_real n1 = nat_to_real n2 -> n1 = n2.

Variable mul_real : real -> real -> real.
Hypothesis mul_nat : forall n1 n2 : nat,
  mul_real n1 n2 = n1 * n2.
Notation "a * b" := (mul_real a b).

Hypothesis mul_comm : forall a b, a * b = b * a.
Hypothesis mul_assoc : forall a b c, (a * b) * c = a * (b * c).

Variable le_real : real -> real -> Prop.
Hypothesis le_nat : forall n1 n2 : nat,
  le_real n1 n2 <-> n1 <= n2.
Notation "a <= b" := (le_real a b).

Definition positive := { x : real | 0 <= x }.

Definition positive_to_real : positive -> real :=
  fun p => proj1_sig p.
Coercion positive_to_real : positive >-> real.

Definition nat_to_positive : nat -> positive :=
  fun n => exist (fun x => 0 <= x) n ltac:(apply le_nat; lia).
Coercion nat_to_positive : nat >-> positive.

Parameter sqrt : positive -> real.
Hypothesis sqrt_pos : forall p,
  0 <= sqrt p.
Hypothesis sqrt_square : forall p,
  sqrt p * sqrt p = p.

Definition rational x :=
  exists p q : nat, x * q = p.

Hypothesis normal_form : forall x,
  rational x ->
  exists p q : nat, x * q = p /\ forall n, prime n -> ~ ((n | q) /\ (n | p)).

Theorem square_two_irrational : ~ rational (sqrt 2).
Proof.

(*
  On suppose (rational (sqrt 2)).
  Décomposer H.

  Search sqrt.

  Search (_ + _ = _ + _ -> _ = _).
*)


  (* Supposons que sqrt 2 est rationel. *)
  intro R.

  (* Supposons p, q, tels que sqrt 2 * q = p (E1) et p, q minimaux (M). *)
  destruct (normal_form _ R) as (p&q&E1&M).

  (* On élève E1 au carré tout en réordonnant les termes (E2). *)
  assert (E2: (sqrt 2 * sqrt 2) * (q * q) = p * p).
  { rewrite <- E1. repeat rewrite mul_assoc. rewrite (mul_comm q (sqrt 2 * q)).
    rewrite mul_assoc. reflexivity. }

  (* On simplifie la racine carré dans E2. *)
  rewrite sqrt_square in E2.
  simpl in E2. repeat rewrite mul_nat in E2.

  (* E2 est maintenant une équation d’entiers naturels et non plus de réels. *)
  apply nat_to_real_inj in E2.

  (* Donc 2 divide p * p (D1). *)
  assert (D1: (2 | p * p)).
  { rewrite <- E2. apply mul_divide. }

  (* Donc 2 divide p (D2) car 2 est premier. *)
  assert (D2: (2 | p)).
  { destruct (prime_divide _ _ _ prime_2 D1); assumption. }

  (* Soit donc p' tel que p = 2 * p' (E3). *)
  pose (D2' := D2). destruct D2' as [p' E3].

  (* On a donc q * q = 2 * p' * p' *)
  assert (E4: (q * q = 2 * p' * p')%nat); [ lia |].

  (* On a donc 2 divide q * q (D3). *)
  assert (D3: (2 | q * q)).
  { exists (p' * p')%nat. lia. }

  (* Donc 2 divide q (D4) car 2 est premier. *)
  assert (D4: (2 | q)).
  { destruct (prime_divide _ _ _ prime_2 D3); assumption. }

  (* On avait supposé p et q minimaux, mais D2 et D4 montrent le contraire. *)
  apply (M 2).
  - apply prime_2.
  - auto.
Qed.

End Sqrt2.

Check square_two_irrational.
