
Tactic Notation "Montrons" "que" constr(x) := assert (x).
Tactic Notation "Appliquons" constr(x) := apply x.
Tactic Notation "On" "choisit" constr(x) := exists x.
Tactic Notation "C'est" "exactement" constr(x) := exact x.
Tactic Notation "On" "pose" constr(x) := pose proof x.

(* Si quelqu'un aime, alors tout le monde l'aime. *)

Parameter personne : Type.
Parameter aime : personne -> personne -> Prop.
Infix "♥" := aime (at level 50).

Axiom tout_le_monde_aime_quelquun_qui_aime :
  forall x, ((exists y, x ♥ y) -> (forall z, z ♥ x)).

Parameter Alice Bob Charlie David : personne.

Axiom Alice_aime_Bob : Alice ♥ Bob.

Theorem Charlie_aime_David : Charlie ♥ David.
Proof.
  Montrons que (forall z, z ♥ Alice).
  - Appliquons tout_le_monde_aime_quelquun_qui_aime.
    On choisit Bob.
    C'est exactement Alice_aime_Bob.
  - On pose (H David).
    Montrons que (forall z, z ♥ David).
    + Appliquons tout_le_monde_aime_quelquun_qui_aime.
      On choisit Alice.
      C'est exactement H0.
    + On pose (H1 Charlie).
      C'est exactement H2.
Qed.
