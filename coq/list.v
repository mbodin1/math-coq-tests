
(* Exemple de « preuve » fausse, refusée par Coq. *)

Require Import List.
Require Import Lia.

Import ListNotations.

Tactic Notation "induction" "over" ident(n) ">=" constr(d) ":" constr(P) :=
  assert (forall n, P (n + d));
  [ induction n;
    try rewrite PeanoNat.Nat.add_0_l;
    try rewrite PeanoNat.Nat.add_0_r;
    try rewrite <- PeanoNat.Nat.add_1_r | ].

Ltac tryfalse :=
  try solve [ exfalso; simpl in *; solve [ lia | lazymatch goal with H : _ |- _ => inversion H end ] ].

(*Tactic Notation "shape" ident(l) "as" *)

(* On veut montrer par récurrence que tous les élements d’une liste sont égaux. *)
Theorem all_equals : forall A (l : list A),
  forall e1 e2, In e1 l /\ In e2 l -> e1 = e2.
Proof.
  induction over n >= 1 : (fun n => forall A (l : list A),
    length l = n ->
    forall e1 e2, In e1 l /\ In e2 l -> e1 = e2).

  (* Cas de base *)
  intros A l E. (* Soit l tel quel length l = 1 *)
  intros e1 e2 (I1&I2). (* Soient e1 et e2 deux éléments de la liste. *)
  (* shape l as [_]. *) repeat (destruct l as [|? l]; tryfalse).
  (* shape e1 as a *) inversion I1; tryfalse; subst.
  (* shape e2 as a *) inversion I2; tryfalse; subst.
  reflexivity.

  (* Cas inductif *)
  intros A l E. (* Soit l tel quel length l = n + 1, avec n >= 1 *)
  (* shape l as ? :: ? :: ?. *) do 2 (destruct l as [|? l]; tryfalse).
  Check (IHn A (a0 :: l) _).
  Check (IHn A (a :: l) _).
  admit.

Admitted.

