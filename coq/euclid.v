
Require Import lib.



Axiom euclid :
  forall p1 p2 : point,
  p1 ≠ p2 ->
  exists! d, p1 \in (d : line) /\ p2 \in d.


Definition colinear p1 p2 p3 :=
  exists d : line, p1 \in d /\ p2 \in d /\ p3 \in d.

Axiom colinear_compose : forall p1 p2 p3 p4,
  colinear p1 p2 p3 /\ colinear p2 p3 p4 -> colinear p1 p2 p4.

Theorem cas_de_base_faux : forall l : list point,
  | l | >= 3 ->
  forall p1 p2 p3,
    p1 \in l /\ p2 \in l /\ p3 \in l ->
    colinear p1 p2 p3.
Proof.
  Par récurrence sur n >= 3,
    on montre que (fun n : nat => forall l p1 p2 p3,
                    | l | = n ->
                      p1 \in l /\ p2 \in l /\ p3 \in l ->
                      colinear p1 p2 p3).
  - admit. (* N’est pas valide ! *)
  - Soient l, p1, p2, p3.
    On suppose (| l | = (n + 4)%nat).
    On suppose (p1 \in l /\ p2 \in l /\ p3 \in l).
    Décomposer (p1 \in l /\ p2 \in l /\ p3 \in l).
    Par analyse de cas sur l.
    Par analyse de cas sur l.
    Par analyse de cas sur l.
    Par analyse de cas sur l.

    apply colinear_compose with (p3 := p).
    split.
    + apply (IHn (p1 :: p2 :: p :: l)).
      * Par calcul.
      * repeat split; repeat ((left; reflexivity) || right).
    + apply (IHn (p2 :: p :: p3 :: l)).
      * Par calcul.
      * repeat split; repeat ((left; reflexivity) || right).

  - Soit l.
    On suppose (| l | >= 3).
    Soient p1, p2, p3.
    apply (L (| l | - 3))%nat.
    Par calcul.
Admitted.












(*

Axiom excluded_middle : forall P, P \/ ~P.

Axiom point_diff : forall p1 : point, exists p2, p1 ≠ p2.

    Par analyse de cas sur (excluded_middle (p1 = p2)).
    + On réécrit p1 en p2.
      Par analyse de cas sur (excluded_middle (p1 = p3)).
      * On réécrit p3 en p2.
        Décomposer (point_diff p1).
        Décomposer (euclid _ _ H6).
        On pose d = d.
        Par calcul.
      * Décomposer (euclid p1 p3 H4).
        On pose d = d.
        Par calcul.
    + Par analyse de cas sur H1.
      * apply colinear_compose with (p3 := p).
        split.
        -- Décomposer (euclid _ _ H2).
           On pose d = d.
           Par calcul.
        -- 
*)
