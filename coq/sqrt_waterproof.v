
Require Import Reals.

From Waterproof Require Export All.
From Waterproof Require Export sup_and_inf sup_and_inf_new_definitions set_definitions.
From Waterproof Require Export AllTactics notations set_notations.

Open Scope R_scope.

Lemma R_complete : ∀ (A : subset_R) (x : A),
    is_bounded_above A ⇒ mk_subset_R (fun M : ℝ => is_sup A M).
Proof.
    Take A : subset_R.
    Take a : A.
    (* Take a : (subset_to_elements_R A). *)
    Assume A_bdd_above : (is_bounded_above A).

    We claim that H : (sig (is_lub (is_in A))).
    Apply completeness.
    Expand the definition of is_bounded_above in A_bdd_above.
    That is, write A_bdd_above as (there exists M, is_upper_bound A M).
    Expand the definition of is_upper_bound in A_bdd_above.
    That is, write A_bdd_above as (there exists M, for all a' : A, a' ≤ M).
    Expand the definition of bound.
    That is, write the goal as (there exists m, Raxioms.is_upper_bound (is_in A) m).
    Choose M such that A_bdd_by_M according to A_bdd_above.
    Choose m := M.
    We need to show that (∀ a : ℝ, is_in A a ⇒ a ≤ M).
    Expand the definition of Raxioms.is_upper_bound.
    That is, write the goal as (for all a', a' ∈ A ⇨ a' ≤ M).
    Take a' : ℝ.
    Assume w : (a' ∈ A).
    (* Define b := (mk_elem_R A a w). *)
    Define b := (mk_element_R (is_in A) a w).
    ltac1:(pose proof (A_bdd_by_M b)).
    This follows by assumption.
    Choose y := x.
    induction y.
    This follows by assumption.
    Choose M such that M_upp_bd according to H.

    destruct (equivalence_sup_lub A M) as [M_is_sup_A H2]. 
    specialize (M_is_sup_A M_upp_bd).
    exists M. 
    exact M_is_sup_A.
Qed.


