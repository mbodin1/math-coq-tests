
\documentclass[aspectratio=169]{beamer}

\usepackage{fontspec}
\setmainfont[Ligatures=TeX]{Linux Libertine O}

\usepackage{tikzpeople}
%\newfontfamily\emoji{Noto Color Emoji}

\usepackage[
	backend=biber,
	style=authoryear
]{biblatex}
\addbibresource{biblio.bib}

\usepackage[normalem]{ulem}

\usepackage{stmaryrd}

\usepackage{svg}

\input{beamer-macros}

\input{mintedSettings}

\usepackage{mathpartir}

\usepackage{hyperref}

\newcommand\emphb{\textbf}

\tikzset{
	box/.style = {
		draw = black,
        fill = white,
		rectangle,
		rounded corners = 2pt,
		text centered,
		minimum height = 5mm,
		minimum width = 10mm
	}
}

\title{LiberAbaci : Mathématiques de licence en Coq}
\author{Martin Bodin}
\date{7 Janvier}

\renewcommand\totalmeaningfulframenumber{11}

\begin{document}

\frame{\titlepage}

%\frame{\tableofcontents}

\sectionframe**{Coq}{
	\begin{center}
		\includegraphics[width=3cm]{images/CoqLogo.png}
	\end{center}
	}

\begin{frame}
	\frametitle{Coq : un logiciel de preuves formelles}

	%\begin{block}{Coq}
		\begin{itemize}
			\item Implémente \anchorm{coc}{}le calcul des constructions,
				\vspace{2em}
			\item Possède une notion précise de preuve mathématique,
			\item<2-> Permet de formaliser \anchorm{consma}{}des constructions mathématiques
				\vspace{4em}
		\end{itemize}
	%\end{block}

	\begin{tikzpicture}[overlay, remember picture]
		\node[below right of = coc, xshift = 5em] (log) {une logique intuitionniste} ;
		\draw[Plum, ->] (coc.south) to [bend right] (log.west) ;

		\only<2->{
			\node[below right of = consma, xshift = 5em] (analysealgebre) {de l’analyse à l’algèbre,} ;
			\node[below right of = consma, xshift = 10.5em, yshift = -3em] (construites) {
				\parbox{8cm}{
					Ces notions doivent être construites dans Coq :
						elles ne sont pas prédéfinies.
						}
					} ;
		\draw[Plum, ->] (consma.south) to [bend right] (analysealgebre.west) ;
		\draw[Plum, ->] (consma) to [bend right] (construites.west) ;
			}
	\end{tikzpicture}

\end{frame}

\begin{frame}
	\frametitle{Ce que Coq n’est pas}

	\begin{block}{Logiciels de calcul numérique}
		R, Octave, Scilab, etc.
	\end{block}

	\vfill

	\begin{block}{Logiciels de calcul symbolique} % Parfois nommé calcul formel.
		Mathematica, Maple, Sympy, SageMath, etc.
	\end{block}

	% À l’oral : exemple de l’intégrale dans ces différents logiciels.
	% Pour R : Évaluation numérique de la fonction en quelques points, et estimation de l’intégrale à partir de cela.
	% Pour Maple : Calcul symbolique automatisé qui va appliquer des intégrations par partie selon des heuristiques pour trouver un résultat lisible.
	% En Coq : Il va nous laisser prouver le théorème d’intégration par partie, puis va nous laisser l’utiliser à condition de lui donner les fonctions à trouver.

\end{frame}

\begin{frame}
	\frametitle{Preuves mathématiques vérifiées en Coq}

		\begin{itemize}
			\item Théorème fondamental de l'algèbre (d'Alembert-Gauss)
				\footfullcite{geuvers2000constructive},
			\item Théorèmes d’incomplétude de G\H{o}del
				\footfullcite{o2005essential},
			\item Indénombrabilité de \(\mathbb{R}\),
				% https://github.com/coq-community/coqtail-math/blob/master/Reals/Logic/Runcountable.v
			\item La série de Leibniz calcule \(\pi\),
				% https://coq.inria.fr/library/Coq.Reals.Ratan.html
			\item Théorème des quatre couleurs
				\footfullcite{gonthier2008formal},
			\item Inégalité de Cauchy-Schwarz,
				% https://github.com/roglo/cauchy_schwarz/blob/master/Cauchy_Schwarz.v
			\item \href{https://madiot.fr/coq100/}{Et beaucoup d’autres !}
		\end{itemize}

\end{frame}

\begin{frame}
	\frametitle{Coq : un logiciel de preuves interactif}

	\vspace{-1ex}

	\begin{centertikz}
		\node [alice, monitor, minimum size = 1cm] (user) {} ;
		\node [right = 3cm of user] (coq) {\includegraphics[width=1cm]{images/CoqLogo.png}} ;
		\node [right = 0cm of user, alice, minimum size = 1cm, transparent, xshift = -5mm] (useranchor) {};
		
		\draw [->, thick, Plum] (coq) to[bend right] node [above] {Buts} (useranchor) ;
		\draw [->, thick, Plum] (useranchor) to[bend right] node [below] {Tactiques} (coq) ;

	\end{centertikz}

	\begin{centertikz}
		\onslide<2-4>{
		\node [locnode green, rectangle round north east = false] at (7, 0) {\(
	\inferrule{
		n: \mathbb{N}
			}{
				P(n)
		}
		\)} ;
		}
		\onslide<3-4>{\node [locnode grey, rectangle round north west = false] at (4, -1.2) {\texttt{induction n}} ;}
		\onslide<4>{
			\node [locnode green, rectangle round north east = false] at (5.7, -2.65) {\(
	\inferrule{
			}{
				P(0) \\\\
				\forall n, P(n) \Rightarrow P(n + 1)
		}
		\)} ;
		}
		\onslide<5->{
		\node [locnode green, rectangle round north east = false] at (5, 0) {\(
	\inferrule{
		\text{Lem}: \forall x, y \in \mathbb{R}, a \in \mathbb{Z}, x \neq 0 \land y \neq 0 \Rightarrow x^a \times y^a = \left(x \times y\right)^a \\\\
		u, v, z: \mathbb{R} \\
		n: \mathbb{Z}
			}{
				z \times \left(u^n \times v^n\right) = \dots
		}
		\)} ;
		}
		\onslide<6->{\node [locnode grey, rectangle round north west = false] at (0, -1.2) {\texttt{rewrite Lem}} ;}
		\onslide<7->{
			\node [locnode green, rectangle round north east = false] at (5, -2.65) {\(
	\inferrule{
		\text{Lem}: \forall x, y \in \mathbb{R}, a \in \mathbb{Z}, x \neq 0 \land y \neq 0 \Rightarrow x^a \times y^a = \left(x \times y\right)^a \\\\
		u, v, z: \mathbb{R} \\
		n: \mathbb{Z}
			}{
				u \neq 0 \\ v \neq 0
				\\\\
				z \times \left(u \times v\right)^n = \dots
		}
		\)} ;
		}
	\end{centertikz}

\end{frame}

\begin{frame}
	\frametitle{Des prémisses faciles à oublier}

		\begin{itemize}
			\item %\texttt{power\_compose\_integers}:
				\(\forall x \in \mathbb{R}, a, b \in \mathbb{N}, \left(x^a\right)^b = x^{ab}\),
			\item %\texttt{power\_compose\_positive}:
				\(\forall x, a, b \in \mathbb{R}, x > 0 \Rightarrow \left(x^a\right)^b = x^{ab}\),
			\item %\texttt{power\_factorise}:
				\(\forall x, y \in \mathbb{R}, a \in \mathbb{Z}, x \neq 0 \land y \neq 0 \Rightarrow x^a \times y^a = \left(x \times y\right)^a\),
			\item %\texttt{power\_multiply}:
				\(\forall x, a, b \in \mathbb{R}, x > 0 \Rightarrow x^a \times x^b = x^{a + b}\)
			\item \dots
		\end{itemize}

\end{frame}

\begin{frame}
	\frametitle{LiberAbaci}

	\begin{block}{Objectif à long terme}
		Utiliser Coq dans un cours de mathématiques de licence.
	\end{block}

	\begin{itemize}
		\item Collaboration avec des enseignents,
		\item Fondements théoriques sur la logique de Coq,
		\item Gestion des structures mathématiques,
		\item Travail sur les notations mathématiques,
		\item Conception de tactiques spécialisées,
		\item Conception d’interface,
		\item Création de bibliothèques mathématiques.
	\end{itemize}

\end{frame}

\begin{frame}
	\frametitle{Avantages possibles de l’utilisation de Coq}

	\begin{itemize}
		\item Preuves \anchorm{correctes}{}correctes par construction,
			\vspace{7em}
		\item Réflexes de preuve,
		\item Retour immédiat de Coq,
		\item L’élève curieux peut explorer comment tout est défini/prouvé.
	\end{itemize}

	\begin{tikzpicture}[overlay, remember picture]
		\node[below right of = correctes, xshift = 5em] (oubli) {impossible d’oublier des cas} ;
		\draw[Plum, ->] (correctes.south) to [bend right] (oubli.west) ;
		\node[below right of = correctes, xshift = 10.5em, yshift = -2.7em] (enonces) {impossible de se tromper dans les énoncés de théorème} ;
		\draw[Plum, ->] (correctes.south) to [bend right] (enonces.west) ;
		\node[below right of = correctes, xshift = 5.2em, yshift = -4.8em] (quantificateurs) {pas d’inversion de \(\forall\) et \(\exists\), etc.} ;
		\draw[Plum, ->] (correctes.south) to [bend right] (quantificateurs.west) ;
	\end{tikzpicture}

\end{frame}

\newcommand\customisability[2]{
	\draw [DarkButter, thick, ->, decorate, decoration = {snake, amplitude = .5pt}] (#1) -- (#2) ;
}

\begin{frame}
	\frametitle{Coq : un logiciel personnalisable}

	\circularPresentation[3cm][locnode yellow][\customisability]%
		{\includegraphics[width=1cm]{images/CoqLogo.png}}{%
		Tactiques,
		Notations,
		\parbox{22mm}{\centering{}Bibliothèques\\Axiomes},
		Interface
		}

\end{frame}

\sectionframe**{Démos}{
	\begin{center}
		\begin{minipage}{3cm}
	\begin{itemize}
		\item \texttt{fermat.v},
		\item \texttt{euclid.v},
		\item \texttt{sqrt.v}.
	\end{itemize}
		\end{minipage}
	\end{center}
	}

\end{document}

