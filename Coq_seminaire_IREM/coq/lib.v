(* Micro-bibliothèque pour l’exposé. *)

(* * Tactiques *)

(* Tactiques d’Arthur pour faire du raisonnement en avant. *)

Require Export LibTactics.

(* Tactiques d’Yves pour structurer la preuve. *)

Ltac Fix name := 
  match goal with
   | |- _ -> _ => fail "statement should be a universal quantification"
   | |- forall _, _ => intros name
   end.

Tactic Notation "Fixes" ident(H1) ident(H2) :=
  Fix H1; Fix H2.
Tactic Notation "Fixes" ident(H1) ident(H2) ident(H3) :=
  Fixes H1 H2; Fix H3.
Tactic Notation "Fixes" ident(H1) ident(H2) ident(H3) ident(H4) :=
  Fixes H1 H2 H3; Fix H4.
Tactic Notation "Fixes" ident(H1) ident(H2) ident(H3) ident(H4) ident(H5) :=
  Fixes H1 H2 H3 H4; Fix H5.

Ltac assume f :=
  match goal with |- _ -> ?c =>
  let x := fresh "assumed_fact" in
  assert (x : f -> c); [intros x | exact x]
  end.

Ltac check :=
  solve[auto | tauto] ||
        fail "I can't manage to check this".

Ltac now_prove f :=
  let x := fresh "now_prove_step" in enough (x : f) by exact x.


(* * Logique *)

(* Importation de la logique classiquei et de l’axiome epsilon d’Hilbert,
  pour donner un environnement plus familier à la démo. *)
From Coq Require Export Logic.Classical Logic.Epsilon.

(* Pour Peano, on a besoin d’un epsilon qui s’utilise facilement. *)

Definition exist_pose [A : Type] [P : A -> Prop] (E : exists a, P a) : A :=
  epsilon (exists_inhabited _ E) P.

Lemma exist_pose_spec : forall A (P : A -> Prop) (E : exists a, P a), P (exist_pose E).
Proof.
  intros. unfold exist_pose. apply epsilon_spec with (P := P). apply E.
Defined.

(* Une tactique pour gérer simplement exist_pose. *)
Tactic Notation "spec_exist_pose" "as" ident(N) :=
  let go E :=
    lets N: (@exist_pose_spec _ _ E) in
  lazymatch goal with
  | |- context [ exist_pose ?E ] => go E
  | H: context [ exist_pose ?E ] |- _ => go E
  end.

Ltac spec_exist_pose :=
  let H := fresh in
  spec_exist_pose as H.

Ltac create_exist_pose_spec d :=
  let rec get_E d :=
    lazymatch d with
    | exist_pose ?E => constr:(E)
    | _ =>
      let h := get_head d in
      let d := eval unfold h in d in
      get_E d
    end in
  let E := get_E d in
  exact (@exist_pose_spec _ _ E).
