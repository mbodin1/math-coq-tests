
(* On importe une petite bibliothèque conçue pour cette démo. *)
Require Import lib.


Declare Scope set_scope.
Delimit Scope set_scope with set.
Open Scope set.


(* La théorie de Zermelo-Fraenkel suppose l’existence de deux notions
  principale : les ensembles, et la relation d’appartenance. *)
Axiom ensemble : Type.
Axiom appartient : ensemble -> ensemble -> Prop.

(* Pour simplifier les notations, on peut indiquer à Coq que certains
  symboles représente par défaut des ensembles. *)
Implicit Type A B C D E F G H I J K L M N O P Q R S T U V W X Y Z : ensemble.

(* On peut alors définir plusieurs notations. *)
Notation "E ≠ F" := (~ E = F) (at level 70) : set_scope.

Notation "E ∈ F" := (appartient E F) (at level 10) : set_scope.
Notation "E \in F" := (E ∈ F) (at level 10, only parsing) : set_scope.

Notation "E ∉ F" := (~ E ∈ F) (at level 10) : set_scope.

(* La relation d’inclusion peut être définie à partir de ∈. *)

Definition inclu E F := forall A, A ∈ E -> A ∈ F.

Notation "E ⊆ F" := (inclu E F) (at level 10) : set_scope.

(* On déclare les différents axiomes de Zermelo-Fraenkel. *)

Axiom extensionalité : forall E F,
  (forall A, A ∈ E <-> A ∈ F) -> E = F.

Axiom paire : forall A B,
  exists E, forall C, C ∈ E <-> (C = A \/ C = B).

(* On peut alors définir l’objet pair et les notations qui vont avec. *)
Definition Paire A B : ensemble := exist_pose (paire A B).

Notation "\{ A , B }" := (Paire A B) (A at level 99, B at level 99) : set_scope.

Lemma Paire_spec A B : forall C, C ∈ \{A, B} <-> C = A \/ C = B.
Proof.
  create_exist_pose_spec \{A, B}.
Qed.

Lemma paire_commute : forall A B, \{A, B} = \{B, A}.
Proof.
  intros A B.
  apply extensionalité.
  intro C.
  repeat rewrite Paire_spec.
  split.
  - intro H.
    destruct H.
    + right.
      apply H.
    + check.
  - check.
Qed.

Definition Singleton A := \{A, A}.

Notation "\{ A }" := (Singleton A) (A at level 99).

Lemma Singleton_spec : forall A C, C ∈ \{ A } <-> C = A.
Proof.
  intros A C.
  unfold Singleton.
  rewrite Paire_spec.
  check.
Qed.

Axiom réunion : forall E,
  exists U, forall A, (A ∈ U <-> exists B, B ∈ E /\ A ∈ B).

Definition Union E := exist_pose (réunion E).

Notation "⋃ E" := (Union E) (at level 8) : set_scope.

Definition Union_spec E : forall A,
    A ∈ ⋃ E <-> (exists B, B ∈ E /\ A ∈ B)
  := ltac:(create_exist_pose_spec (⋃ E)).

Definition Add E A := ⋃ \{E, \{ A }}.

Lemma Add_spec : forall E A C, C ∈ (Add E A) <-> C ∈ E \/ C = A.
Proof.
  intros E A C.
  unfold Add.
  rewrite Union_spec.
  split.
  - intros (B&I1&I2).
    rewrite Paire_spec in I1.
    destruct I1 as [BE|BA].
    + subst.
      left.
      exact I2.
    + subst.
      rewrite Singleton_spec in I2.
      check.
  - intros [CE|CA].
    + exists E.
      rewrite Paire_spec.
      check.
    + exists \{ A }.
      rewrite Paire_spec.
      split.
      * check.
      * rewrite Singleton_spec.
        check.
Qed.

Axiom parties : forall E,
  exists P, forall A, (A ∈ P <-> A ⊆ E).

Definition Parties E := exist_pose (parties E).

Notation "'𝒫' E" := (Parties E) (at level 5) : set_scope.

Definition Parties_spec E : forall A,
    A ∈ 𝒫 E <-> A ⊆ E
  := ltac:(create_exist_pose_spec (𝒫 E)).

Axiom compréhension : forall E (P : ensemble -> Prop),
  exists F, forall A, (A ∈ F <-> A ∈ E /\ P A).

Definition Filtre E (P : ensemble -> Prop) := exist_pose (compréhension E P).

Notation "E 'tq' P" := (Filtre E P) (at level 5) : set_scope.
Notation "{ x ∈  E | H }" := (E tq fun x => H) (x at level 9, E at level 99) : set_scope.

Definition Filtre_spec E (P : ensemble -> Prop) : forall A,
    A ∈ E tq P <-> A ∈ E /\ P A
  := ltac:(create_exist_pose_spec (E tq P)).

Definition Intersection A B := { x ∈ A | x ∈ B }.

Notation "A ∩ B" := (Intersection A B) (at level 50) : set_scope.

Lemma intersection_commute : forall A B, A ∩ B = B ∩ A.
Proof.
  intros A B.
  apply extensionalité.
  intro C.
  unfold Intersection.
  repeat rewrite Filtre_spec.
  check.
Qed.

Axiom vide : exists V, forall A, A ∉ V.

Definition Vide := exist_pose vide.

Notation "'Ø'" := Vide (at level 0) : set_scope.

Notation "{ A , .. , B }" := (Add .. (Add Ø B) .. A) : set_scope.

Definition ensemble_1 := {Ø, {Ø, {Ø}}, Ø, Ø}.
Definition ensemble_2 := {Ø, {Ø, {Ø}}}.

Lemma ensemble_1_2 : ensemble_1 = ensemble_2.
Proof.
  apply extensionalité.
  intro A.
  unfold ensemble_1, ensemble_2.
  repeat rewrite Add_spec.
  check.
Qed.

Axiom infini : exists X, Ø ∈ X /\ forall A, A ∈ X -> {A} ∈ X.

Axiom remplacement : forall (P : ensemble -> ensemble -> Prop) A,
  (forall x, x ∈ A -> exists! y, P x y) ->
  exists I, forall x, x ∈ A -> exists y, y ∈ I /\ P x y.

Axiom fondation : forall A,
  A ≠ Ø -> exists B, B ∈ A /\ A ∩ B = Ø.
