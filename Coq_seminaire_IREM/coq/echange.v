
Hypothesis P : nat -> Prop.

Lemma lem : forall x, P(x) -> P(x).
Proof.
  intro x.
  intro p.
  exact p.
Qed.
